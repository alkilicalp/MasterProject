﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerObjectData : MonoBehaviour
{
    public TriggerType triggerType;
    public bool singleTrigger;
    public float triggerDistance;
    public int duration;
    public int cooldown;
}
